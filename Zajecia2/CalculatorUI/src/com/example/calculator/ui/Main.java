package com.example.calculator.ui;

import com.example.calculator.basic.ExtendedCalculator;
import com.example.calculator.logic.ExtendedCircleCalculator;
import com.example.calculator.logic.ExtendedRectangleCalculator;
import com.example.calculator.logic.ExtendedSquareCalculator;
import com.example.calculator.logic.ExtendedTriangleCalculator;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
     ExtendedCalculator square = new ExtendedSquareCalculator();
     ExtendedCalculator circle = new ExtendedCircleCalculator();
     ExtendedCalculator triangle = new ExtendedTriangleCalculator();
     ExtendedCalculator rectangle= new ExtendedRectangleCalculator();
     boolean whileControler = true;
     while(whileControler) {
            System.out.printf("podaj numer figury do obliczenia obwodu: \n (1)Kwadrat \n (2)Kolo \n (3)Trojkat \n (4)Rownoleglobok\n" +
                    "obliczenia pola:\n (5)Kwadrat \n (6)Kolo \n (7)Trojkat \n (8)Rownoleglobok\n  (0)Wyjscie\n\n");
            Scanner userInput = new Scanner(System.in);
            int geometricalFigureNumber = userInput.nextInt();
            switch (geometricalFigureNumber) {
                case 1: System.out.printf("Obwod kwadratu to: %f \n", square.calculatePerimeter());
                break;
                case 2:  System.out.printf("Obwod kola to: %f \n", circle.calculatePerimeter());
                break;
                case 3:   System.out.printf("Obwod trojkonta to: %f \n", triangle.calculatePerimeter());
                break;
                case 4:    System.out.printf("Obwod rownolegloboku to: %f \n", rectangle.calculatePerimeter());
                break;
                case 5: square.calculateArena();
                    break;
                case 6:  circle.calculateArena();
                    break;
                case 7:   triangle.calculateArena();
                    break;
                case 8:    rectangle.calculateArena();
                    break;
                case 0:     System.out.printf("Wyjscie \n");
                            whileControler=false;
                    break;
                default:    System.out.printf("Zla liczba!\n");
            }
        }



    }
}
