package com.example.calculator.logic;

import com.example.calculator.basic.ExtendedCalculator;

public class ExtendedRectangleCalculator extends RectangleCalculator implements ExtendedCalculator {

    @Override
    public void calculateArena() {
        System.out.printf("Pole rownolegloboku: %f\n",rectangleWidth*rectangleHeight);
    }
}
