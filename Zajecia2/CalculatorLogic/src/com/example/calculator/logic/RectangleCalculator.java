package com.example.calculator.logic;

import com.example.calculator.basic.Calculator;

public class RectangleCalculator implements Calculator {
    double rectangleHeight=5;
    double rectangleWidth=10;
    @Override
    public double calculatePerimeter() {


        return 2*rectangleHeight+2*rectangleWidth;
    }
}
