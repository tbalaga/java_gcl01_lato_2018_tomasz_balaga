package com.example.calculator.logic;

import com.example.calculator.basic.ExtendedCalculator;

public class ExtendedTriangleCalculator extends TriangleCalculator implements ExtendedCalculator {
    @Override
    public void calculateArena() {
        System.out.printf("Obwod trojkata: %f\n",0,5*triangleBaseSize*triangleHeight);
    }
}
