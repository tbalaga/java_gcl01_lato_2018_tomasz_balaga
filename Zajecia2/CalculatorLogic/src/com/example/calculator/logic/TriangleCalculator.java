package com.example.calculator.logic;

import com.example.calculator.basic.Calculator;

public class TriangleCalculator implements Calculator {
    double triangleBaseSize=10;
    double triangleSideSize1=10;
    double triangleSideSize2=10;
    double triangleHeight=8.6602540378444;
    @Override
    public double calculatePerimeter() {

        return triangleBaseSize+triangleSideSize1+triangleSideSize2;
    }
}
