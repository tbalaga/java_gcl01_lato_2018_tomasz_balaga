package com.example.calculator.logic;

import com.example.calculator.basic.Calculator;

public class CircleCalculator implements Calculator {
    double circleRadius=10;
    @Override
    public double calculatePerimeter() {
        return 2*3.14*circleRadius;
    }
}
