package com.example.calculator.logic;

import com.example.calculator.basic.Calculator;

public class SquareCalculator implements Calculator {
    double squareSideSize = 10;
    @Override
    public double calculatePerimeter() {

        return 4*squareSideSize;
    }
}
