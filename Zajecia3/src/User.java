public class User {
    private long id;
    private String name;
    private long numberOfStars;
    private long numberOfSentMessgages;

    public void setNumberOfStars(long numberOfStars) {
        this.numberOfStars = numberOfStars;
    }

    public void setNumberOfSentMessgages(long numberOfSentMessgages) {
        this.numberOfSentMessgages = numberOfSentMessgages;
    }

    public long getNumberOfStars() {
        return numberOfStars;
    }

    public long getNumberOfSentMessgages() {
        return numberOfSentMessgages;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }


    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }


}
