import java.util.HashMap;

public class Main {

    public static void main(String[] args)  {
        User newUser= new User();
        newUser.setId(1);
        newUser.setName("Marcin");
        try {
            ChatEngine.loginUser(newUser);
        } catch (UserLoginExeption userLoginExeption) {
            userLoginExeption.printStackTrace();
        }
    }
}
