import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.Map;

public class ChatEngine {
    static long  numberOfUsers=0;
    private static HashMap<Long,User> myMap=new HashMap();
    private static HashMap<String,User>myMapString=new HashMap();
    public static void loginUser(User user) throws UserLoginExeption {
        if(hasUser(user.getName()))
        {
            throw new UserLoginExeption();
        }



        numberOfUsers++;
        user.setId(numberOfUsers);
        myMap.put(user.getId(),user);



    }
    public static void logoutUser(long userId) throws  UserRemoveExeption
    {
        if(!myMap.containsKey(userId))
        {
            throw new UserRemoveExeption("No user with this id!");
        }
        else{
            String usrName = myMap.get(userId).getName();
            myMap.remove(userId);
            myMapString.remove(usrName);

        }


    }
    public static boolean hasUser(String userName)
    {
        if(myMap.containsValue(userName))
        return true;
        else return false;
    }

    public static boolean hasId(long userId)
    {
        if(myMap.containsValue(userId))
            return true;
        else return false;
    }

    public static int getNumbeOfUsers()
    {
        return myMap.size();
    }
    public static void  addUserStar(String userName)
    {
        long numberOfStars=myMapString.get(userName).getNumberOfStars();
        numberOfStars++;
        myMapString.get(userName).setNumberOfStars(numberOfStars);
    }
    public static void removeUserStar(String userName)
    {
        long numberOfStars=myMapString.get(userName).getNumberOfStars();
        numberOfStars--;
        myMapString.get(userName).setNumberOfStars(numberOfStars);

    }


}
