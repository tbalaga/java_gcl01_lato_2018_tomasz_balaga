package com.balaga;


import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class Main {


    public static void main(String[] args) throws IOException {
        ScatterSystem scatterSystem = new ScatterSystem();
        SystemCache cache = new SystemCache();
        ExtendedSystemCache extendedSystemCache = new ExtendedSystemCache();
        String csvFile = "C:\\Users\\Tomasz\\IdeaProjects\\Zajecia8\\src\\result.csv";
        Random random = new Random();
        double[] input = new double[10];
        for (int i = 0; i < 10; i++) {
            input[i] = random.nextDouble();
            Double output = cache.get(input);

            if (output == null) {
                output = scatterSystem.makeOperation(input);
                cache.put(input, output);
                System.out.println(output);
            }
        }

        extendedSystemCache.exportCSV(csvFile);

    }
}
