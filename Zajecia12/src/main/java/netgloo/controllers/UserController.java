package netgloo.controllers;


import netgloo.models.*;
//import netgloo.models.ClientService;
//import netgloo.models.CarDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class UserController {
//
//    @Autowired
//    private ClientService clientService;

    public void getModel(Model model) {
        Iterable<Car> carsAllFromDataBase = carDao.findAll();
        Iterable<Offer> offersFromDataBase = offerDao.findAll();
        Iterable<Client> clientsFromDataBase = clientDao.findAll();
        model.addAttribute("cars", carsAllFromDataBase);
        model.addAttribute("offers", offersFromDataBase);
        model.addAttribute("clients", clientsFromDataBase);
        model.addAttribute("car", new Car());
        model.addAttribute("client", new Client());
        model.addAttribute("offer", new Offer());
    }


    @RequestMapping("/index")
    public String setupFrom(Model model) {
        getModel(model);
        return "index";
    }

    @RequestMapping(value = "/client", method = RequestMethod.POST)
    public String addClient(Model model, @ModelAttribute Client client) {
        try {
            clientDao.save(client);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        getModel(model);
        return "index";
    }

    @RequestMapping(value = "/car", method = RequestMethod.POST)
    public String addCar(Model model, @ModelAttribute Car insertedCar) {
        try {
            carDao.save(insertedCar);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        getModel(model);
        return "index";
    }

    @RequestMapping(value = "/offer", method = RequestMethod.POST)
    public String addOffer(Model model, @ModelAttribute Offer offer) {
        try {
            offerDao.save(offer);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        getModel(model);
        return "index";
    }

    @RequestMapping(value = "/client/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public String deleteClient(Model model, @PathVariable long id) {
        try {
            Client client = new Client(id);
            clientDao.delete(client);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        getModel(model);
        return "index";
    }

    @RequestMapping(value = "/car/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public String deleteCar(Model model, @PathVariable long id) {
        try {
            Car car = new Car(id);
            carDao.delete(car);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        getModel(model);
        return "index";
    }

    @RequestMapping(value = "/offer/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public String deleteOffer(Model model, @PathVariable long id) {
        try {
            Offer offer = new Offer(id);
            offerDao.delete(offer);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        getModel(model);
        return "index";
    }

    @RequestMapping(value = "/client/{id}")
    public String clinetChangePage(Model model, @PathVariable long id){
        model.addAttribute("client", clientDao.findOne(id));
        return "updateClient";
    }

    @RequestMapping(value = "/car/{id}")
    public String carChangePage(Model model, @PathVariable long id){
        model.addAttribute("car", carDao.findOne(id));
        return "updateCar";
    }

    @RequestMapping(value = "/offer/{id}")
    public String offerChangePage(Model model, @PathVariable long id){
        model.addAttribute("offer", offerDao.findOne(id));
        return "updateOffer";
    }

    @RequestMapping("/update/cars/{id}")
    @ResponseBody
    public String updateCar(@PathVariable long id, @ModelAttribute Car car) {
        try {
            car.setId(id);
            carDao.save(car);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "index";
    }


    @Autowired
    private CarDao carDao;

    @Autowired
    private ClientDao clientDao;

    @Autowired
    private OfferDao offerDao;


//
//    @RequestMapping(value = "/client.do", method = RequestMethod.POST)
//    public String doActions(@ModelAttribute Client client, BindingResult result, @RequestParam String action, Map<String, Object> map) {
//        Client client1 = new Client();
//        switch (action.toLowerCase()) {
//            case "add":
//                clientService.add(client);
//                client1 = client;
//                break;
//
//            case "edit":
//
//                clientService.edit(client);
//                client1 = client;
//                break;
//
//            case "delete":
//
//                clientService.delete((int) client.getId());
//                client1 = new Client();
//                break;
//
//            case "search":
//
//                Client searchClient = clientService.getClient((int) client.getId());
//                client1 = searchClient !=null ? searchClient : new Client();
//                break;
//        }
//        map.put("client", client1);
//        map.put("clientList", clientService.getAllClient());
//        return "client";
//    }


//    @RequestMapping("/create")
//    @ResponseBody
//    public String create(String email, String name) {
//        Client client = null;
//        try {
//            client = new Client(email, name);
//            userDao.save(client);
//        } catch (Exception ex) {
//            return "Error creating the client: " + ex.toString();
//        }
//        return "Client successfully created! (id = " + client.getId() + ")";
//    }

//    @RequestMapping("/delete")
//    @ResponseBody
//    public String delete(long id) {
//        try {
//            Client client = new Client(id);
//            userDao.delete(client);
//        } catch (Exception ex) {
//            return "Error deleting the user: " + ex.toString();
//        }
//        return "Client successfully deleted!";
//    }


//    @RequestMapping("/get-by-email")
//    @ResponseBody
//    public String getByEmail(String email) {
//        String userId;
//        try {
//            Client client = userDao.findByEmail(email);
//            userId = String.valueOf(client.getId());
//        } catch (Exception ex) {
//            return "Client not found";
//        }
//        return "The user id is: " + userId;
//    }

//    @RequestMapping("/update")
//    @ResponseBody
//    public String updateUser(long id, String email, String name) {
//        try {
//            Client client = userDao.findOne(id);
//            client.setEmail(email);
//            client.setName(name);
//            userDao.save(client);
//        } catch (Exception ex) {
//            return "Error updating the user: " + ex.toString();
//        }
//        return "Client successfully updated!";
//    }


}
