package netgloo.models;

import org.springframework.data.repository.CrudRepository;

public interface OfferDao extends CrudRepository<Offer, Long> {

}
