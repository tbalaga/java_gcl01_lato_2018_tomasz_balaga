package netgloo.models;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ClientDao extends CrudRepository<Client, Long> {

}
