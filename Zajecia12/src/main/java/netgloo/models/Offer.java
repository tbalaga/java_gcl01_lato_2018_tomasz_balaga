package netgloo.models;


import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "offer")
public class Offer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    public Offer() {

    }

    public Offer(String date, Integer value) {

        this.date = date;
        this.value = value;
    }

    @NotNull
    private String date;

    public Offer(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    @NotNull
    private Integer value;

}
