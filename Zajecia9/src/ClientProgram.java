import java.io.DataInputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

public class ClientProgram {
    public static void main(String[] args) throws IOException, InterruptedException {
        InetSocketAddress address = new InetSocketAddress("localhost", 80);
        SocketChannel client = SocketChannel.open(address);

        byte[] helloWorldFrame = ByteBuffer.allocate(1024).putInt(CustomServer.ECHO).array();
        ByteBuffer writeBuffer = ByteBuffer.wrap(helloWorldFrame);
        writeBuffer.put("companyName".getBytes());
        client.write(writeBuffer);
        writeBuffer.clear();

        ByteBuffer readBuffer = ByteBuffer.allocate(400);
        int read = client.read(readBuffer);
        if (read > 0) {
            char letter = readBuffer.getChar();
            System.out.print(letter);
        }

    }
}
