import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashSet;
import java.util.Set;


public class CustomServer {
    public static final int PING_FRAME = 1;
    public static final int HELLO_WORLD_FRAME = 2;
    public static final int ECHO = 3;

    private Set<Thread> threads = new HashSet<>();

    public void run(int port) throws IOException {
        if (threads.size() > 5) {
            return;
        }
        ServerSocket serverSocket = new ServerSocket(80);
        Socket socket = serverSocket.accept();
        Thread thread = new Thread(() -> doClientLogic(socket));
        thread.run();
        threads.add(thread);
    }

    public void stop() {
        for (Thread thread : threads) {
            thread.interrupt();
        }
    }

    private void doClientLogic(Socket socket) {

        try {
            DataInputStream input = new DataInputStream(socket.getInputStream());
            DataOutputStream output = new DataOutputStream(socket.getOutputStream());

            while (!Thread.interrupted()) {
                if (input.available() > 0) {
                    int readInt = input.readInt();
                    System.out.println("Read int: " + String.valueOf(readInt));
                    switch (readInt) {
                        case PING_FRAME:
                            output.writeInt(PING_FRAME); // odsyłamy typ ramki (pusta ramka)
                            output.flush();
                            break;
                        case HELLO_WORLD_FRAME:
                            output.writeInt(HELLO_WORLD_FRAME); // typ ramki
                            output.writeUTF("HELLO WORLD!"); // dane ramki
                            output.flush();
                            break;
                        case ECHO:
                            String sentString = input.readUTF();
                            output.writeUTF(sentString); // dane ramki
                            output.flush();
                            break;
                        //next cases…
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (!socket.isClosed()) {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        threads.remove(Thread.currentThread());
    }


// pewien kod po stronie serwera


}
