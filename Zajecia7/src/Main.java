import java.util.Timer;
import java.util.TimerTask;

public class Main {

    public static void main(String[] args) {

        Worker worker = new Worker("Kajetan");
        worker.setListener(new WorkerListener() {
            @Override
            public void onWorkerStarted() {
                System.out.println("Worker Started! ");
            }

            @Override
            public void onWorkerStopped() {
                System.out.println("Worker Stopped! ");

            }

            @Override
            public void onTaskStarted(int taskNumber, String taskName) {
                System.out.println("Task Started! ");
            }

            @Override
            public void onTaskCompleted(int taskNumber, String taskName) {
                System.out.println("Task Completed! ");
            }
        });
        worker.enqueueTask("firstTask", taskNumber -> {
            System.out.println("First Task!\n");
            long currentTimeMillis = System.currentTimeMillis();
            while (System.currentTimeMillis() <= currentTimeMillis + 10000) {

            }

        });
        worker.enqueueTask("SecondTask", taskNumber -> {

            Thread.sleep(10000);
            System.out.println("SecondTask!\n");
        });
        worker.enqueueTask("", taskNumber -> {

            System.out.println("Third Task!\n");
            long currentTimeMillis = System.currentTimeMillis();
            while (System.currentTimeMillis() <= currentTimeMillis + 10000) {

            }
        });

        worker.enqueueTask("FourthTask", taskNumber -> {
            // yield powoduje, ze dany watek nic nie robi ale inne watki moga korzystac z procesora
            long currentTimeMillis = System.currentTimeMillis();
            while (System.currentTimeMillis() <= currentTimeMillis + 10000) {
                Thread.yield();
            }
            System.out.println("Fourth Task!\n");
        });
        worker.enqueueTask("FifthTask", taskNumber -> {
            System.out.println("Fifth Task!\n");
            long currentTimeMillis = System.currentTimeMillis();
            while (System.currentTimeMillis() <= currentTimeMillis + 10000) {

            }
        });
        worker.start();

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                worker.stop();

            }
        }, 15000);


        while (worker.isWorking()) {
            Thread.yield();
        }
        System.out.println("FINISH!");
    }


}
