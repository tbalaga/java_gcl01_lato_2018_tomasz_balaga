package sample;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.ListView;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;


import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class Controller {

    private DateFormat hoursDateFormat = new SimpleDateFormat("HH");
    private DateFormat minutesDateFormat = new SimpleDateFormat("mm");
    private DateFormat secondsDateFormat = new SimpleDateFormat("ss");
    DateFormat hoursMinutesDateFormat = new SimpleDateFormat("HH:mm");


    private ObservableList items = FXCollections.observableArrayList();
    private String alarmFile = "alarmSound.mp3";
    private Media alarm;
    private MediaPlayer mediaPlayer;

    private Font hourMinutesFont;
    private Font secondsFont;
    @FXML
    private ListView<String> alarmList;

    @FXML
    private Canvas clockCanvas;

    @FXML
    void initialize() {

        hourMinutesFont = Font.loadFont("file:resources/fonts/digital-7.ttf", 100);
        secondsFont = Font.loadFont("file:resources/fonts/digital-7.ttf", 35);
        GraphicsContext gc = clockCanvas.getGraphicsContext2D();
        drawFrame(gc);
        Timeline timeline = new Timeline(new KeyFrame(Duration.millis(100), actionEvent -> Controller.this.drawFrame(gc)));
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
        Timeline timelineMusic = new Timeline(new KeyFrame(Duration.minutes(1), ae -> checkAlarms()));
        timelineMusic.setCycleCount(Animation.INDEFINITE);
        timelineMusic.play();
    }

    private void checkAlarms() {

        Date date = new Date();
        String msg = hoursMinutesDateFormat.format(date);

        if(items.contains(msg))
        {
            alarm = new Media(new File(alarmFile).toURI().toString());
            mediaPlayer = new MediaPlayer(alarm);
            mediaPlayer.play();
        }

    }

    private void drawFrame(GraphicsContext graphicsContext) {

        Date date = new Date();
        final String hours = hoursDateFormat.format(date);
        final String minutes = minutesDateFormat.format(date);
        final String seconds = secondsDateFormat.format(date);

        StringBuilder currentTimeBuilder = new StringBuilder();

        currentTimeBuilder.append(hours);
        if (date.getTime() / 10 % 2 == 0) {
            currentTimeBuilder.append(":");
        } else {
            currentTimeBuilder.append(" ");
        }
        currentTimeBuilder.append(minutes);

        String currentTime = currentTimeBuilder.toString();

        Text hourMinutesText = new Text(currentTime);
        hourMinutesText.setFont(hourMinutesFont);

        Text secondsText = new Text(seconds);
        secondsText.setFont(secondsFont);

        graphicsContext.clearRect(0, 0, clockCanvas.getWidth(), clockCanvas.getHeight());
        graphicsContext.setFont(hourMinutesFont);
        graphicsContext.fillText(currentTime, clockCanvas.getWidth() / 3 - hourMinutesText.getLayoutBounds().getWidth() / 3, clockCanvas.getHeight() / 2 + hourMinutesText.getLayoutBounds().getHeight() / 2);
        graphicsContext.setFont(secondsFont);
        graphicsContext.fillText(seconds, clockCanvas.getWidth() / 2 + hourMinutesText.getLayoutBounds().getWidth() / 2, clockCanvas.getHeight() / 2 + hourMinutesText.getLayoutBounds().getHeight() / 2);

    }

    public void deleteAlarm(ActionEvent event)
    {
        if(alarmList.getSelectionModel().getSelectedIndex() >= 0) {
            items.remove(alarmList.getSelectionModel().getSelectedIndex());
            alarmList.refresh();
        }
    }

    public void deleteAllAlarms(ActionEvent event)
    {
        items.clear();
        alarmList.getItems().clear();
        alarmList.refresh();

    }


    public void closeProgram(ActionEvent actionEvent) {

        Platform.exit();
        System.exit(0);

    }


    public void aboutAuthor(ActionEvent actionEvent) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("sample1.fxml"));
        Parent root = (Parent) fxmlLoader.load();
        Stage primaryStage = new Stage();
        primaryStage.setTitle("Author");
        primaryStage.setScene(new Scene(root, 400, 600));
        primaryStage.show();
    }

    public void addAlarmButton(ActionEvent actionEvent) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("sample2.fxml"));
        Parent root = (Parent) fxmlLoader.load();

        Controller2 controller = fxmlLoader.getController();
        controller.setItems(items);
        controller.setList(alarmList);

        Stage primaryStage = new Stage();
        primaryStage.setTitle("Add Alarm");
        primaryStage.setScene(new Scene(root, 300, 300));
        primaryStage.show();
    }


}
