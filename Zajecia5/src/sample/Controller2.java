package sample;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Controller2 {
    private ObservableList items;
    private ListView<String> alarmList;
    Pattern pattern = Pattern.compile("[0-1]{1}[0-9]{1}:[0-5]{1}[0-9]{1}");
    Pattern pattern2 = Pattern.compile("[2]{1}[0-3]{1}:[0-5]{1}[0-9]{1}");
    @FXML
    private TextField alarmAddTextField;

    @FXML
    private Label alarmErrorLabel;

    public void validate()
    {

        Matcher matcher = pattern.matcher(alarmAddTextField.getCharacters());
        Matcher matcher2 = pattern2.matcher(alarmAddTextField.getCharacters());

        if((matcher.find()|| matcher2.find())&& alarmAddTextField.getCharacters().length() == 5) {
            alarmErrorLabel.setVisible(false);

            items.add(alarmAddTextField.getText());
            alarmList.setItems(items.sorted());
        }
        else
            alarmErrorLabel.setVisible(true);
    }

    public void setItems(ObservableList items2)
    {
        items = items2;
    }

    public void setList(ListView<String> alarmList2)
    {
        alarmList = alarmList2;
    }
}
